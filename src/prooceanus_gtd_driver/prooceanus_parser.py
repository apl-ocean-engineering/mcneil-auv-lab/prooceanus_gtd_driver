from typing import List, Optional
import rospy

import prooceanus_msgs.msg


def mems_from_tokens(tokens: List[str]) -> Optional[prooceanus_msgs.msg.Mems]:
    mems = prooceanus_msgs.msg.Mems()
    try:
        mems.c1, mems.c2, mems.c3, mems.c4, mems.c5, mems.c6 = map(int, tokens[0:6])
        mems.raw_temperature, mems.raw_pressure = map(int, tokens[6:8])
        mems.temperature, mems.pressure = map(float, tokens[8:10])

    except Exception as ex:
        rospy.logerr(
            "{} \n Unable to parse tokens into MEMS message: {}".format(ex, tokens)
        )
        return None

    return mems


def parse_gtd_bytes(data: bytes) -> Optional[prooceanus_msgs.msg.Gtd]:
    """
    String received from the ProOcanus is CSV (ASCII, utf-8 encoded into raw bytes)

    If parsing fails, returns None. Does not raise.

    If parsing succeeds, returns Gtd message. Header will be blank.

    Example data string from sensor:
    "P M,TDG-CM00010,37638,37221,24146,23915,32198,28477,8272346,7217708,21.006807,1104.406616,28.150,37407,35447,24121,22665,32123,28778,8231914,6812688,20.289062,1013.148254,28.150,11.8,0.0,0.0,1957242639\r\n"

    The final 3 tokens are [reserved, reserved, checksum]
    """
    try:
        # We want decode errors to raise a UnicodeDecodeError, rather than trying
        # to recover and potentially introducing corrupt data.
        data_str = data.decode("utf-8", errors="strict")
    except Exception as ex:
        # I think UnicodeDecodeError is the only exception decode raises, but I'm not
        # positive about that, so go ahead and catch anything.
        rospy.logerr(
            "{} \n Unable to decode bytes into utf-8! {}".format(ex, list(data))
        )
        return None

    tokens = data_str.split(",")
    num_fields = 28  # magic number -- expected number of fields in a message.
    if len(tokens) != num_fields:
        rospy.logerr(
            "Incorrect number of fields in data. expected {}, got {}: input = {}. ".format(
                num_fields, len(tokens), data_str
            )
        )
        return None

    msg = prooceanus_msgs.msg.Gtd()

    msg.instrument_id = tokens[1]

    mems1 = mems_from_tokens(tokens[2:12])
    mems2 = mems_from_tokens(tokens[13:23])
    if mems1 is None or mems2 is None:
        return None
    msg.mems1 = mems1
    msg.mems2 = mems2

    try:
        msg.v1 = float(tokens[12])
        msg.v2 = float(tokens[23])
        msg.supply_voltage = float(tokens[24])
    except Exception as ex:
        rospy.logerr("{} \n Unable to decode voltage values: {}".format(ex, tokens))
        return None

    return msg
