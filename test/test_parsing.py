import unittest
import numpy as np

from prooceanus_gtd_driver import parse_gtd_bytes
import prooceanus_msgs.msg


class TestParsing(unittest.TestCase):
    # Example of valid string from Craig's documentation
    docs_example = "P M,TDG-CM00010,38554,35719,24886,23212,32193,28616,8020378,6806058,12.232523,1055.127319,25.594,38560,35796,24890,23255,32383,28495,8096756,6244324,13.260146,851.426391,25.756,13.6,0.0,0.0,3571183516\r\n".encode(
        "utf-8"
    )

    # Example of valid string from my playing with the instrument
    gtd_example = "P M,TDG-CM00010,37638,37221,24146,23915,32198,28477,8272346,7217708,21.006807,1104.406616,28.150,37407,35447,24121,22665,32123,28778,8231914,6812688,20.289062,1013.148254,28.150,11.8,0.0,0.0,1957242639\r\n".encode(
        "utf-8"
    )

    def test_good_input(self):
        for input in [self.docs_example, self.gtd_example]:
            msg = parse_gtd_bytes(input)
            self.assertIsInstance(msg, prooceanus_msgs.msg.Gtd)

    def test_invalid_bytes(self):
        """
        0x9c, 0xa5 Are both examples of invalid unicode bytes

        Insert an undecodeable byte at a random position of the input, confirm
        that the parser catches the error.
        """
        for _ in np.arange(5):
            bad_bytearray = bytearray(self.docs_example)
            idx = np.random.randint(len(bad_bytearray))
            bad_bytearray[idx] = 0x9C
            bad_input = bytes(bad_bytearray)
            msg = parse_gtd_bytes(bad_input)
            self.assertIsNone(msg)

    def test_too_few_tokens(self):
        # simply removed single field from docs_example
        input_data = "P M,TDG-CM00010,38554,35719,24886,23212,32193,28616,8020378,6806058,12.232523,1055.127319,25.594,35796,24890,23255,32383,28495,8096756,6244324,13.260146,851.426391,25.756,13.6,0.0,0.0,3571183516\r\n".encode(
            "utf-8"
        )
        msg = parse_gtd_bytes(input_data)
        self.assertIsNone(msg)

    def test_too_many_tokens(self):
        # repeated field in docs_example
        input_data = "P M,TDG-CM00010,38554,35719,24886,23212,32193,28616,8020378,6806058,12.232523,1055.127319,25.594,38560,35796,24890,23255,32383,28495,8096756,6244324,13.260146,851.426391,25.756,25.756,13.6,0.0,0.0,3571183516\r\n".encode(
            "utf-8"
        )
        msg = parse_gtd_bytes(input_data)
        self.assertIsNone(msg)

    def test_wrong_types(self):
        # Turned an int into a float in docs_example

        input_data = "P M,TDG-CM00010,38.554,35719,24886,23212,32193,28616,8020378,6806058,12.232523,1055.127319,25.594,38560,35796,24890,23255,32383,28495,8096756,6244324,13.260146,851.426391,25.756,13.6,0.0,0.0,3571183516\r\n".encode(
            "utf-8"
        )
        msg = parse_gtd_bytes(input_data)
        self.assertIsNone(msg)

    def test_invalid_voltage(self):
        input_data = "P M,TDG-CM00010,38554,35719,24886,23212,32193,28616,8020378,6806058,12.232523,1055.127319,25.594,38560,35796,24890,23255,32383,28495,8096756,6244324,13.260146,851.426391,25.756,v2,0.0,0.0,3571183516\r\n".encode(
            "utf-8"
        )
        msg = parse_gtd_bytes(input_data)
        self.assertIsNone(msg)


if __name__ == "__main__":
    unittest.main()
