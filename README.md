## prooceanus_gtd_driver

This driver for a ProOceanus MiniTDGP instrument is specialized to
handle Craig McNeil's custom firmware that spits out more data than
the default fields specified in the documentation.

These instruments are configured to run at 9600 baud and to simply
spit data out on the serial port, no automated configuration needed OR desired.


### testing

For now, testing is manual:

`catkin test prooceanus_gtd_driver`

Or, for test coverage:
~~~python
coverage run test/test_parsing.py
coverage html
firefox htmlcov/index.html
~~~

TODO: Figure out
* run tests in Gitlab CI
* Confirm 100% test coverage in Gitlab CI
* rostest that tests the ROS node initialization
